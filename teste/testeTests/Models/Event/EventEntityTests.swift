//
//  EventEntityTests.swift
//  teste
//
//  Created by Luiz Aires Soares on 05/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Quick
import Nimble
@testable
import teste
import Mapper

class EventEntityTests: QuickSpec {
    
    override func spec() {
        var fakeEvent: EventEntity?
        
        let fakeEventDict: [String : AnyObject] = [
            "id": "115615" as AnyObject,
            "name": "Festival Path 2017"  as AnyObject,
            "url": "https://test.sympla.com.br/festival-path-2017__115615"  as AnyObject,
            "logo_url": "https://d1gkntzr8mxq7s.cloudfront.net/fe_58b715e0a867e.png"  as AnyObject,
            "start_date": "2017-05-06 07:00:00"  as AnyObject,
            "event_type": "NORMAL"  as AnyObject,
            "location": [
                "name": "Rua dos Coropés, 88"  as AnyObject,
                "address": "Rua dos Coropés"  as AnyObject,
                "address_num": "88"  as AnyObject,
                "address_alt": "Credenciamento"  as AnyObject,
                "neighborhood": "Pinheiros"  as AnyObject,
                "city": "São Paulo"  as AnyObject,
                "state": "SP"  as AnyObject,
                "state_desc": "São Paulo"  as AnyObject,
                "zip_code": "05426-010"  as AnyObject
            ] as AnyObject
        ]
        
        context("Testing parsing EventEntity") {
            beforeEach {
                fakeEvent = EventEntity.from(fakeEventDict as NSDictionary)
            }
            it("") {
                expect(fakeEvent).toNot(beNil())
            }
        }
        
    }
    
}
