//
//  LocationEntityTests.swift
//  teste
//
//  Created by Luiz Aires Soares on 05/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Quick
import Nimble
@testable
import teste
import Mapper

class LocationEntityTests: QuickSpec {
    
    override func spec() {
        var fakeLocation: LocationEntity?
        
        let fakeLocationDict: [String : AnyObject] = [
                "name": "Rua dos Coropés, 88" as AnyObject,
                "address": "Rua dos Coropés" as AnyObject,
                "address_num": "88" as AnyObject,
                "address_alt": "Credenciamento" as AnyObject,
                "neighborhood": "Pinheiros" as AnyObject,
                "city": "São Paulo" as AnyObject,
                "state": "SP" as AnyObject,
                "state_desc": "São Paulo" as AnyObject,
                "zip_code": "05426-010" as AnyObject
            ]
        
        context("Testing parsing LocationEntity") {
            beforeEach {
                fakeLocation = LocationEntity.from(fakeLocationDict as NSDictionary)
            }
            it("") {
                expect(fakeLocation).toNot(beNil())
            }
        }
        
    }
    
}
