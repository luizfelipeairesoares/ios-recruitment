//
//  EventServiceTests.swift
//  teste
//
//  Created by Luiz Aires Soares on 05/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Quick
import Nimble
@testable
import teste
import Moya

class EventServiceTests: QuickSpec {
    
    override func spec() {
        
        let fakeEndpointClosure = { (target: SymplaAPI) -> Endpoint<SymplaAPI> in
            let url = target.baseURL.appendingPathComponent(target.path).absoluteString
            let endpoint: Endpoint<SymplaAPI> = Endpoint<SymplaAPI>(url: url,
                                                                    sampleResponseClosure: { .networkResponse(200, target.sampleData) },
                                                                    method: target.method,
                                                                    parameters: target.parameters,
                                                                    parameterEncoding: target.parameterEncoding,
                                                                    httpHeaderFields: nil)
            return endpoint
        }
        
        var fakeProvider: MoyaProvider<SymplaAPI>!
        
        beforeEach {
            fakeProvider = MoyaProvider<SymplaAPI>(endpointClosure: fakeEndpointClosure, stubClosure: MoyaProvider<SymplaAPI>.immediatelyStub)
        }
        
        context("Testing featured events") {
            var called = false
            beforeEach {
                fakeProvider.request(.featured, completion: { _ in
                    called = true
                })
            }
            it("") {
                expect(called).to(beTrue())
            }
        }
        
        context("Testing highlighted") {
            var called = false
            beforeEach {
                fakeProvider.request(.highlighted, completion: { _ in
                    called = true
                })
            }
            it("") {
                expect(called).to(beTrue())
            }
        }
        
    }
    
}

