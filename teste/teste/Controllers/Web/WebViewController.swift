//
//  WebViewController.swift
//  teste
//
//  Created by Luiz Aires Soares on 05/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, ControllersProtocol {
    
    @IBOutlet weak var web: UIWebView!
    
    fileprivate var url: URL?
    
    fileprivate var alreadyFinishLoading: Bool = false
    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        if let u = self.url {
            self.web.loadRequest(URLRequest(url: u))
        } else {
            self.showError(message: "Não foi possível carregar a página :(")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Auxiliar functions
    
    internal func assignURL(url: URL) {
        self.url = url
    }
    
    internal func assignTitle(eventTitle: String?) {
        if let title = eventTitle {
            self.title = title
        } else {
            self.title = "Detalhe Evento"
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - WebViewController Extension - UIWebViewDelegate

extension WebViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        if !self.alreadyFinishLoading {
            self.showLoading()
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if !webView.isLoading {
            self.hideLoading()
            self.alreadyFinishLoading = true
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.hideLoading()
        self.showError(message: error.localizedDescription)
    }
    
}
