//
//  HighlightCollectionViewCell.swift
//  teste
//
//  Created by Luiz Aires Soares on 04/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import UIKit
import Kingfisher

class HighlightCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblState: UILabel!
    
    // MARK: - Cell Functions
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.img.image = UIImage(named: "img_placehoder")
    }
    
}

extension HighlightCollectionViewCell: CellProtocol {
    
    func configureCell(obj: AnyObject?) {
        if let event = obj as? EventEntity {
            if let url = event.logoURL {
                self.img.kf.setImage(with: url, placeholder: UIImage(named: "img_placeholder"))
            } else {
                self.img.image = UIImage(named: "img_placeholder")
            }
            self.lblName.text = event.name
            self.lblLocation.text = event.location?.name
            self.lblTime.text = event.eventHour() + "h" 
            self.lblDay.text = event.eventDay()
            self.lblMonth.text = event.eventMonth().uppercased()
            
            var address = ""
            if let city = event.location?.city {
                address = city
            }
            if let state = event.location?.state {
                address += ", \(state)"
            }
            self.lblState.text = address
        }
    }
    
}
