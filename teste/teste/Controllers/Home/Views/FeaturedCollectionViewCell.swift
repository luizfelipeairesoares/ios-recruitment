//
//  FeaturedCollectionViewCell.swift
//  teste
//
//  Created by Luiz Aires Soares on 04/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import UIKit
import Kingfisher

class FeaturedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btn: UIButton!
    
    // MARK: - Cell Functions
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.img.contentMode = .scaleAspectFill
        self.img.image = UIImage(named: "img_placehoder")
    }
    
}

// MARK: - FeaturedCollectionViewCell CellProtocol

extension FeaturedCollectionViewCell: CellProtocol {
    
    func configureCell(obj: AnyObject?) {
        if let event = obj as? EventEntity {
            if let imgURL = event.logoURL {
                self.img.kf.setImage(with: imgURL,
                                     placeholder: UIImage(named: "img_placeholder"),
                                     completionHandler: { [unowned self] (image, error, cache, url) in
                                        if error == nil && image != nil {
                                            self.img.image = image!
                                            self.img.contentMode = .scaleAspectFill
                                        }
                })
            } else {
                self.img.image = UIImage(named: "img_placeholder")
            }
        }
    }
}
