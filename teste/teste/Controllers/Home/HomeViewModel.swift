//
//  HomeViewModel.swift
//  teste
//
//  Created by Luiz Aires Soares on 04/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import UIKit

protocol HomeProtocol: ControllersProtocol {
    
    func reloadData()
    func pushWebController(url: URL, eventTitle: String?)
    
}

class HomeViewModel {
    
    private var highlighted: [EventEntity] = []
    private var featured: [EventEntity] = []
    
    internal var data: [[EventEntity]] = [[], []]
    
    internal var eventNumber: String?

    private weak var controller: HomeProtocol?
    
    internal func assignController(controller: HomeProtocol) {
        self.controller = controller
    }
    
    // MARK: - Requests functions
    
    internal func requestItems() {
        self.controller?.showLoading()
        self.requestFeatured { [unowned self] in
            self.requestHighlighted()
        }
    }
    
    private func requestFeatured(completion: @escaping (() -> Void)) {
        EventService().requestFeaturedEvents { [unowned self] (result) in
            switch result {
            case .success(let data):
                if let events = data.data {
                    self.featured = events
                }
                self.eventNumber = data.totalPublished
                completion()
            case .failure(_):
                completion()
            }
        }
    }
    
    private func requestHighlighted() {
        EventService().requestHighlightedEvents { [unowned self] (result) in
            switch result {
            case .success(let data):
                if let events = data.data {
                    self.highlighted = events
                }
                
                
                if let number = self.eventNumber {
                    var total = Int(number)!
                    total += Int(data.totalPublished!)!
                    self.eventNumber = "\(total)"
                } else {
                    self.eventNumber = data.totalPublished
                }
                
                self.data.removeAll()
                self.data.insert(self.featured, at: 0)
                self.data.insert(self.highlighted, at: 1)
                self.controller?.hideLoading()
                self.controller?.reloadData()
            case .failure(let msg):
                self.controller?.showError(message: msg)
            }
        }
    }
    
    // MARK: - Auxiliar Functions
    
    internal func showFeaturedEventDetail(eventAtIndex: Int) {
        let event = self.featured[eventAtIndex]
        self.showEventDetail(event: event)
    }
    
    internal func showHighlightedEventDetail(eventAtIndex: Int) {
        let event = self.highlighted[eventAtIndex]
        self.showEventDetail(event: event)
    }
    
    private func showEventDetail(event: EventEntity) {
        if let url = event.url {
            self.controller?.pushWebController(url: url, eventTitle: event.name)
        } else {
            self.controller?.showError(message: "O evento não possui detalhe ainda :(")
        }
    }
    
}
