//
//  HomeViewController.swift
//  teste
//
//  Created by Luiz Aires Soares on 04/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var lblEvent: UILabel!
    @IBOutlet weak var collectionFeatured: UICollectionView!
    @IBOutlet weak var lblDestack: UILabel!
    @IBOutlet weak var collectionHighlight: UICollectionView!
    
    fileprivate var viewmodel: HomeViewModel?
    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewmodel = HomeViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.viewmodel?.assignController(controller: self)
        self.viewmodel?.requestItems()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button Actions
    
    @IBAction func btnTouched(_ sender: UIButton) {
        self.showError(message: "Você curtiu este evento!")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - HomeViewController Extension - HomeProtocol

extension HomeViewController: HomeProtocol {
    
    func reloadData() {
        self.collectionFeatured.reloadData()
        self.collectionHighlight.reloadData()
        if let number = self.viewmodel?.eventNumber {
            let boldSans = UIFont(name: "OpenSans-Bold", size: 20.0)
            let regularSans = UIFont(name: "OpenSans", size: 20)
            let eventAttrStr = NSAttributedString(string: "\(number) eventos.", attributes: [NSForegroundColorAttributeName : UIColor.red,
                                                                                             NSFontAttributeName : boldSans ?? UIFont.boldSystemFont(ofSize: 20)])
            let experienceStr = NSAttributedString(string: " Viva as melhores experiências por todo o Brasil.",
                                                   attributes: [NSForegroundColorAttributeName : UIColor.black,
                                                                NSFontAttributeName : regularSans ?? UIFont.systemFont(ofSize: 20)])
            let mutable = NSMutableAttributedString(attributedString: eventAttrStr)
            mutable.append(experienceStr)
            self.lblEvent.attributedText = mutable
            self.lblEvent.numberOfLines = 0
        } else {
            self.lblEvent.text = "Viva as melhores experiências por todo o Brasil."
        }
    }
    
    func pushWebController(url: URL, eventTitle: String?) {
        let web = self.storyboard?.instantiateViewController(withIdentifier: WebViewController.storyboardID()) as! WebViewController
        web.assignURL(url: url)
        web.assignTitle(eventTitle: eventTitle)
        self.navigationController?.pushViewController(web, animated: true)
    }
}

// MARK: - HomeViewController Extension - UICollectionViewDataSource

extension HomeViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionHighlight {
            return self.viewmodel?.data[1].count ?? 0
        } else {
            return self.viewmodel?.data[0].count ?? 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionFeatured {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeaturedCollectionViewCell.cellIdentifier(), for: indexPath) as! FeaturedCollectionViewCell
            let event = self.viewmodel?.data[0][indexPath.row]
            cell.configureCell(obj: event as AnyObject)
            cell.btn.addTarget(self, action: #selector(btnTouched(_:)), for: .touchUpInside)
            return cell
         } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HighlightCollectionViewCell.cellIdentifier(), for: indexPath) as! HighlightCollectionViewCell
            let event = self.viewmodel?.data[1][indexPath.row]
            cell.configureCell(obj: event as AnyObject)
            cell.btn.addTarget(self, action: #selector(btnTouched(_:)), for: .touchUpInside)
            return cell
        }
    }
    
}

// MARK: - HomeViewController Extension - UICollectionViewDelegate 

extension HomeViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if collectionView == self.collectionFeatured {
            self.viewmodel?.showFeaturedEventDetail(eventAtIndex: indexPath.row)
        } else {
            self.viewmodel?.showHighlightedEventDetail(eventAtIndex: indexPath.row)
        }
    }
    
}

// MARK: - HomeViewContorller Extension - UICollectionViewFlowLayoutDelegate

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionFeatured {
            return CGSize(width: collectionView.frame.size.width-32, height: collectionView.frame.size.height)
        } else {
            return CGSize(width: collectionView.frame.size.width-64, height: collectionView.frame.size.height)
        }
    }
    
    
}


