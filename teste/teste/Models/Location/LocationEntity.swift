//
//  LocationEntity.swift
//  teste
//
//  Created by Luiz Aires Soares on 03/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Foundation
import Mapper

struct LocationEntity: Mappable {
    
    let name:           String?
    let address:        String?
    let number:         String?
    let alt:            String?
    let neighborhood:   String?
    let city:           String?
    let state:          String?
    let stateLong:      String?
    let zipCode:        String?
    
    init(map: Mapper) throws {
        self.name           = map.optionalFrom("name")
        self.address        = map.optionalFrom("address")
        self.number         = map.optionalFrom("address_num")
        self.alt            = map.optionalFrom("address_alt")
        self.neighborhood   = map.optionalFrom("neighborhood")
        self.city           = map.optionalFrom("city")
        self.state          = map.optionalFrom("state")
        self.stateLong      = map.optionalFrom("state_desc")
        self.zipCode        = map.optionalFrom("zip_code")
        
    }
    
}
