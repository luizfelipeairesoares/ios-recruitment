//
//  EventEntity.swift
//  teste
//
//  Created by Luiz Aires Soares on 03/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Foundation
import Mapper

enum eventType: String {
    case normal     = "NORMAL"
}

struct EventEntity: Mappable {
    
    let id:         String?
    let name:       String?
    let url:        URL?
    let logoURL:    URL?
    let startDate:  Date?
    let type:       eventType?
    let location:   LocationEntity?
    
    init(map: Mapper) throws {
        self.id         = map.optionalFrom("id")
        self.name       = map.optionalFrom("name")
        self.url        = map.optionalFrom("url")
        self.logoURL    = map.optionalFrom("logo_url")
        self.type       = map.optionalFrom("event_type")
        self.location   = map.optionalFrom("location")
        self.startDate  = map.optionalFrom("start_date", transformation: { (value) -> Date? in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return dateFormatter.date(from: value as! String)
        })
    }
    
    internal func eventDay() -> String {
        if let date = self.startDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd"
            return dateFormatter.string(from: date)
        } else {
            return ""
        }
    }
    
    internal func eventMonth() -> String {
        if let date = self.startDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM"
            return dateFormatter.string(from: date)
        } else {
            return ""
        }
    }
    
    internal func eventHour() -> String {
        if let date = self.startDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH"
            return dateFormatter.string(from: date)
        } else {
            return ""
        }
    }
    
}
