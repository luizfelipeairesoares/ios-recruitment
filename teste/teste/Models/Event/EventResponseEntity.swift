//
//  EventResponseEntity.swift
//  teste
//
//  Created by Luiz Aires Soares on 03/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Foundation
import Mapper

struct EventResponseEntity: Mappable {
    
    let totalPublished:     String?
    let total:              Int?
    let data:               [EventEntity]?
    
    init(map: Mapper) throws {
        self.totalPublished     = map.optionalFrom("total_published")
        self.total              = map.optionalFrom("total")
        self.data               = map.optionalFrom("data")
    }
    
}
