//
//  CellProtocol.swift
//  teste
//
//  Created by Luiz Aires Soares on 04/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Foundation
import UIKit

protocol CellProtocol {
    
    static func cellIdentifier() -> String
    func configureCell(obj: AnyObject?)
    
}

extension CellProtocol where Self: UICollectionViewCell {
    
    static func cellIdentifier() -> String {
        return "\(self)Identifier"
    }
    
}
