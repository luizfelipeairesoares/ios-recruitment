//
//  ControllersProtocol.swift
//  teste
//
//  Created by Luiz Aires Soares on 04/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Foundation
import PKHUD

protocol ControllersProtocol: class {
    static func storyboardID() -> String
    func showLoading()
    func hideLoading()
    func showError(message: String)
    func showOptionAlert(message: String, handler: @escaping ((UIAlertAction) -> Void))
}

extension ControllersProtocol where Self: UIViewController {
    
    static func storyboardID() -> String {
        return "\(self)StoryboardID"
    }
    
    func showLoading() {
        HUD.show(.progress, onView: self.view)
    }
    
    func hideLoading() {
        if UIApplication.shared.isNetworkActivityIndicatorVisible == false {
            HUD.flash(.labeledSuccess(title: nil, subtitle: nil), delay: 0.2)
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func showError(message: String) {
        HUD.hide()
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showOptionAlert(message: String, handler: @escaping ((UIAlertAction) -> Void)) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "YES", style: .default, handler: handler))
        alertController.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
