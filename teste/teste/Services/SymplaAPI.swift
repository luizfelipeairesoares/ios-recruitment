//
//  SymplaAPI.swift
//  teste
//
//  Created by Luiz Aires Soares on 03/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Foundation
import Moya

enum SymplaAPI {
    
    case featured
    case highlighted
    
}

extension SymplaAPI: TargetType {
    
    var baseURL: URL {
        return URL(string: "https://s3.amazonaws.com/sympla-ios-recruitment/endpoints/") ?? URL(string: "")!
    }
    
    var path: String {
        switch self {
        case .featured:
            return "featured_events.json"
        case .highlighted:
            return "highlighted_events.json"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var parameters: [String : Any]? {
        return nil
    }
    
    var parameterEncoding: ParameterEncoding {
        return URLEncoding()
    }
    
    var sampleData: Data {
        switch self {
        case .featured:
            return stubbedResponse("featured_events")
        default:
            return stubbedResponse("highlighted_events")
        }
    }
    
    var task: Task {
        return .request
    }
    
}

func stubbedResponse(_ filename: String) -> Data! {
    @objc class TestClass: NSObject { }
    
    let bundle = Bundle(for: TestClass.self)
    let path = bundle.path(forResource: filename, ofType: "json")
    return (try? Data(contentsOf: URL(fileURLWithPath: path!)))
}
