//
//  Errors.swift
//  teste
//
//  Created by Luiz Aires Soares on 04/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Foundation

enum Errors: Error {
    case jsonMappingError
    case userUnauthorizedError
    case requestUnauthorizedError
    case requestNotFoundError
    case undefinedError(description: String)
    
    func message() -> String {
        switch self {
        case .jsonMappingError:
            return "Não foi possível Mapear o JSON"
        case .userUnauthorizedError:
            return "Usuário não autorizado"
        case .requestUnauthorizedError:
            return "Requisição não autorizada"
        case .requestNotFoundError:
            return "Não pudemos encontrar o que foi pedido"
        case .undefinedError(let description):
            return description
        }
    }
    
    static let allErrors = [jsonMappingError,
                            userUnauthorizedError,
                            requestUnauthorizedError,
                            requestNotFoundError,
                            undefinedError(description: "Ops, algo deu errado!")]
}
