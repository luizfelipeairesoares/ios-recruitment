//
//  BaseService.swift
//  teste
//
//  Created by Luiz Aires Soares on 04/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Foundation
import Moya
import Moya_ModelMapper
import Mapper

protocol BaseService {
    
    func handleResponseObject<T: Mappable>(response: Moya.Response, key: String?) throws -> T
    func handleResponseArray<T: Mappable>(response: Moya.Response, key: String?) throws -> [T]
    func handleResponseError(response: Moya.Response) -> String
    
    func handleFailure(failure: Moya.Error) -> String
    
    func handleResponseCookies(response: Moya.Response)
}

extension BaseService {
    
    func handleResponseObject<T: Mappable>(response: Moya.Response, key: String? = nil) throws -> T {
        do {
            let obj: T = try response.mapObject(withKeyPath: key) as T
            return obj
        } catch {
            throw Errors.jsonMappingError
        }
    }
    
    func handleResponseArray<T: Mappable>(response: Moya.Response, key: String? = nil) throws -> [T] {
        do {
            let obj: [T] = try response.mapArray(withKeyPath: key) as [T]
            return obj
        } catch {
            throw Errors.jsonMappingError
        }
    }
    
    func handleResponseError(response: Moya.Response) -> String {
        do {
            let err: [String : Any] = try response.mapJSON() as! [String : Any]
            if (err["msg"] != nil) {
                return err["msg"] as! String
            } else {
                return self.handleError(code: response.statusCode)
            }
        } catch {
            return self.handleError(code: response.statusCode)
        }
    }
    
    func handleFailure(failure: Moya.Error) -> String {
        guard let response = failure.response else {
            return self.handleError(code: failure._code)
        }
        do {
            let json = try response.mapJSON()
            // TODO: MAP ERROR FROM API
            return "\(json)"
        } catch {
            return self.handleError(code: response.statusCode)
        }
    }
    
    func handleResponseCookies(response: Moya.Response) {
        if let resp = response.response as? HTTPURLResponse {
            if let fields = resp.allHeaderFields as? [String : String] {
                if let url = resp.url {
                    let cookies = HTTPCookie.cookies(withResponseHeaderFields: fields, for: url)
                    print(cookies)
                }
            }
        }
    }
    
    private func handleError(code: Int) -> String {
        switch (code) {
        case 401:
            return Errors.userUnauthorizedError.message()
        case 403:
            return Errors.requestUnauthorizedError.message()
        case 404:
            return Errors.requestNotFoundError.message()
        default:
            return Errors.undefinedError(description: "Ops! Algo deu errado.").message()
        }
    }
}
