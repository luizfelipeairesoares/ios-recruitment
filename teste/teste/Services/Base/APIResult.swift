//
//  APIResult.swift
//  teste
//
//  Created by Luiz Aires Soares on 04/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Foundation

enum APIResult<T, String> {
    case success(T)
    case failure(String)
}
