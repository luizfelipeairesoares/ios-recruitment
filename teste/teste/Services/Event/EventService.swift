//
//  EventService.swift
//  teste
//
//  Created by Luiz Aires Soares on 04/03/17.
//  Copyright © 2017 sympla. All rights reserved.
//

import Foundation
import Moya

struct EventService: BaseService {
    
    private let provider: MoyaProvider<SymplaAPI> = MoyaProvider<SymplaAPI>(endpointClosure: { (target: SymplaAPI) -> Endpoint<SymplaAPI> in
        let url = target.baseURL.appendingPathComponent(target.path).absoluteString
        let endpoint: Endpoint<SymplaAPI> = Endpoint<SymplaAPI>(url: url,
                                                                sampleResponseClosure: { .networkResponse(200, target.sampleData) },
                                                                method: target.method,
                                                                parameters: target.parameters,
                                                                parameterEncoding: target.parameterEncoding,
                                                                httpHeaderFields: nil)
        return endpoint
        
    }, plugins: [NetworkLoggerPlugin(verbose: true)])
    
    // MARK: - Functions
    
    internal func requestHighlightedEvents(completion: @escaping (APIResult<EventResponseEntity, String>) -> Void) {
        self.provider.request(.highlighted) { (response) in
            switch response {
            case .success(let result):
                do {
                    let entity: EventResponseEntity = try result.mapObject() as EventResponseEntity
                    completion(APIResult.success(entity))
                } catch {
                    completion(APIResult.failure(self.handleResponseError(response: result)))
                }
            case .failure(let error):
                completion(APIResult.failure(self.handleFailure(failure: error)))
            }
        }
    }
    
    internal func requestFeaturedEvents(completion: @escaping (APIResult<EventResponseEntity, String>) -> Void) {
        self.provider.request(.featured) { (response) in
            switch response {
            case .success(let result):
                do {
                    let entity: EventResponseEntity = try result.mapObject() as EventResponseEntity
                    completion(APIResult.success(entity))
                } catch {
                    completion(APIResult.failure(self.handleResponseError(response: result)))
                }
            case .failure(let error):
                completion(APIResult.failure(self.handleFailure(failure: error)))
            }
        }
    }
}
